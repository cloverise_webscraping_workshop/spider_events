# -*- coding: utf-8 -*-
import dateutil.parser
import json
import scrapy
from events.items import EventsItem

class MeetupSpider(scrapy.Spider):
    name = "meetup"
    allowed_domains = ["meetup.com", '192.168.2.82']
    start_urls = (
        'http://www.meetup.com/find/events/?allMeetups=false&keywords=&radius=100&lat=31.23&lon=121.47&eventFilter=mysugg',
    )

    def parse(self, response):
        for elem in response.selector.xpath('//ul[@class="event-listing-container"]/li'):
            item = EventsItem()
            item['title'] = elem.xpath('.//*[@itemprop="summary"]/text()').extract().pop()
            item['start_date'] = dateutil.parser.parse(elem.xpath('.//*[@itemprop="startDate"]/@datetime').extract().pop()).isoformat()
            yield item
            yield self.make_requests_for_publish(item)

    def make_requests_for_publish(self, item):
        return scrapy.Request(url='http://192.168.2.82:3000/api/post_result',
                              method='POST',
                              body=json.dumps(dict(item)),
                              headers={'content-type':'application/json'},
                              callback=self.parse_publish)

    def parse_publish(self, response):
        print response.body_as_unicode()
